import yaml
import schedule
import time
from sso import SSO
from esi import ESI
from status import Printer
from discordhooker import Hooker
from cacheout import Cache
from apiqueue import ApiQueue
cache = Cache()
# Sissy

# hook1="https://discordapp.com/api/webhooks/448989938508234772/fxpX5eFBgL55nabtTjt4tZPQi5jpLe7n8XjsEWzR8JS5aQtN-a-EXUPw-fWwMfuF0uOD"
# hook2= "https://discordapp.com/api/webhooks/450789933653950474/7A2i0o68Vca7G60oFZcNa22SgmuCBrrDsEOOe7W8zNrTSNzBReKRj7DMhkrfOyao8Dni"

# TQ
# You can add as many hooks as you like.
hook1="https://discordapp.com/api/webhooks/451703617653702656/wsrSQZ2uiXPdlY7wH8ZX7VTYjENBVq1Q-1Kr8Tz_EC1Xcea0Rawm7h9iV564B3rwImVS"
hook2="https://discordapp.com/api/webhooks/450855820125798411/52iOJ2xbCweX9HUjo3DPm6vgfWQtnc7e7FKNWbe0bIeZvm89mli5euc64Uvf4r2FyXbX"
hook3="https://discordapp.com/api/webhooks/477237790241849355/Gg9ZbP-w8UFMJPKomW4BL6exvhHVawLYMkuGzTy0dP6rWSnICdKGyKgRp-o5Vnn6ha70"
hook4="https://discordapp.com/api/webhooks/504659236932943883/KxZ2X5NIKLeomawWFJI0_KaoxOOKkK7xKNZRICrdcVXacai4ploeXCildeOzKq6NdYU4"

notification_options={'whitelist': [
    'StructureUnderAttack',
    'StructureFuelAlert',
    'StructureLostShields',
    'StructureLostArmor',
    'AllWarDeclaredMsg',
    'AllWarInvalidatedMsg',
]}
discord = {
    'personal': {
        'token': 'your token here',
        'channel_id': 'your channel id here'
    }
}
sso_app = {
    'client_id': 'your client id here',
    'secret_key': 'your secret key here'
}

eve_apis = {
    'char1': {
        'character_name': 'char1',
        'character_id': char id,
        'refresh_token': 'refresh token here'
    },
    'char 2': {
        'character_name': 'char2',
        'character_id': char id,
        'refresh_token': 'refresh token here'
    },
}

def asso():
    return SSO(
        sso_app['client_id'],
        sso_app['secret_key'],
        eve_apis['char 2']['refresh_token'],
        eve_apis['char 2']['character_id']
    )

def api_to_sso(api):
    return SSO(
        sso_app['client_id'],
        sso_app['secret_key'],
        api['refresh_token'],
        api['character_id']
    )

aq = ApiQueue(list(map(api_to_sso, eve_apis.values())))
def noti(notifications):
    if notification_options['whitelist']:
        notifications = [notification for notification in notifications if notification['type']
         in notification_options['whitelist']]
    return notifications


def gethook1(data):
    stru = ['StructureUnderAttack', 'StructureLostShields', 'StructureLostArmor']
    k = ''
    for i in range(0, len(data)):
        if data[i]['type'] in stru and data[i]['notification_id'] not in cache:
            k=k+(Printer(ESI(asso())).transform(data[i]))
            cache.set(data[i]['notification_id'], k)
            k=k+'\n'
    if k != '':
        Hooker(hook1).ping(k)
        Hooker(hook2).ping(k)
    else:
        print (time.strftime(" %d:%m:%y %H:%M:%S", time.gmtime()))
        return ""


def gethook2(data):
    stru = ['StructureFuelAlert']
    k = ''
    for i in range(0, len(data)):
        if data[i]['type'] in stru and data[i]['notification_id'] not in cache:
            k=k+(Printer(ESI(asso())).transform(data[i]))
            cache.set(data[i]['notification_id'], k)
            k=k+'\n'
    if k != '':
        Hooker(hook2).ping(k)
    else:
        print (time.strftime(" %d:%m:%y %H:%M:%S", time.gmtime()))
        return ""

def getwar(data):
    stru = ['AllWarDeclaredMsg' ,'AllWarInvalidatedMsg']
    k = ''
    for i in range(0, len(data)):
        if data[i]['type'] in stru and data[i]['notification_id'] not in cache:
            k=k+(Printer(ESI(asso())).transform(data[i]))
            cache.set(data[i]['notification_id'], k)
            k=k+'\n'
    if k != '':
        Hooker(hook3).ping(k)
        Hooker(hook1).ping(k)
        Hooker(hook4).ping(k)
    else:
        print (time.strftime(" %d:%m:%y %H:%M:%S", time.gmtime()))
        return ""

def pingping(data):
    getwar(data)
    gethook1(data)
    gethook2(data)


def silly():
    pingping(noti(ESI(aq.get()).get_new_notifications()))


schedule.every(5).minutes.do(silly)
while True:
    schedule.run_pending()
    time.sleep(1)
